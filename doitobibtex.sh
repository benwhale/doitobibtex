#! /bin/sh
#
# See 
# http://matt.might.net/articles/bash-by-example/
# for bash programming.
#
# For documationi on the url see
# https://citation.crosscite.org/docs.html


curl -LH "Accept: application/x-bibtex" https://doi.org/$1
printf "\n"

