# doitobibtex

Provides a bash and python script to interogate doi.org, 
The scripts request the data associated to a doi in bibtex format.
The python script requires the `requests` module and python 3. 
The code is so simple I leave modifications to the user if these requirements are
not appropriate.

The python script also contains, unused, code for requesting the same information 
directly from crossref. In principle doi.org should forward the request to the
appropriate doi registar.

