#! /usr/bin/python3
#
# Shamelessly inspired by https://academia.stackexchange.com/questions/15504/is-there-an-open-source-tool-for-producing-bibtex-entries-from-paper-pdfs
#
# See also:
# https://citation.crosscite.org/docs.html

import argparse
import requests


def get_using_crossref(doi):
    crossref_url = "https://api.crossref.org/works/{doi}/transform/application/x-bibtex"
    r = requests.get(
        crossref_url.format(doi=args.doi)
    )
    return r.text

def get_using_doi(doi):
    doi_url = "https://doi.org/{doi}"
    headers = {
        'Accept': "application/x-bibtex"
    }
    r = requests.get(
        doi_url.format(doi=args.doi),
        headers=headers
    )
    return r.text

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('doi', help='the doi of the article')

    args = parser.parse_args()
    print(get_using_doi(args.doi))

